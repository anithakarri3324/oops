import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class MouseListenerExample {
    public static void main(String[] args) {
        // Create a JFrame window
        JFrame frame = new JFrame("MouseListener Example");
        // Create a JPanel
        JPanel panel = new JPanel();
        // Set the size of the panel
        panel.setPreferredSize(new Dimension(300, 200));
        // Create a label
        JLabel label = new JLabel("No mouse event");
        // Add the label to the panel
        panel.add(label);
        // Create a MouseListener
        MouseListener mouseListener = new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                label.setText("Mouse Clicked");
            }
            public void mousePressed(MouseEvent e) {
                label.setText("Mouse Pressed");
            }
            public void mouseReleased(MouseEvent e) {
                label.setText("Mouse Released");
            }
            public void mouseEntered(MouseEvent e) {
                label.setText("Mouse Entered");
            }
            public void mouseExited(MouseEvent e) {
                label.setText("Mouse Exited");
            }
        };
        // Add the MouseListener to the panel
        panel.addMouseListener(mouseListener);
        // Add the panel to the frame
        frame.getContentPane().add(panel);
        // Set frame properties
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
